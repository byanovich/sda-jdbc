package ee.sda.jdbc;

import java.sql.*;

public class JDBCStatements {

    private Connection connection;

    private Statement statement;

    public static void main(String[] args) throws SQLException {
        JDBCStatements jdbcStatements = new JDBCStatements();
        jdbcStatements.insertJob("Assistant Manager", 5000.0, 10000.0);
        // jdbcStatements.insertBulkJobs();
        jdbcStatements.updateJob("Assistant of Assistant Manager", 28);
        jdbcStatements.deleteJob("Assistant Manager");
        jdbcStatements.selectJobs();
    }

    public JDBCStatements() throws SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sda",
                "sdauser", "password");
        statement = connection.createStatement();
//        connection.commit();
//        connection.rollback();
        System.out.println("Auto commit: " + connection.getAutoCommit());
        connection.setAutoCommit(false);
    }

    private void selectJobs() {
//        Statement statement = connection.createStatement();
        try {
            ResultSet resultSet =
                    statement.executeQuery("SELECT job_id, job_title, min_salary, max_salary FROM jobs");

            while (resultSet.next()) {
                int jobId = resultSet.getInt(1);
                String jobTitle = resultSet.getString(2);
                double minSalary = resultSet.getDouble(3);
                double maxSalary = resultSet.getDouble(4);

                System.out.println("JOB: id=" + jobId + " title=" + jobTitle + " minSalary=" + minSalary
                        + " maxSalary=" + maxSalary);
            }

            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                // e1.printStackTrace();
            }
        }
    }

    private void insertJob(String title, double minSalary, double maxSalary) throws SQLException {
//        Statement statement = connection.createStatement();
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO jobs (job_title, min_salary, max_salary) " +
                "VALUES(?, ?, ?)");
        preparedStatement.setString(1, title);
        preparedStatement.setDouble(2, minSalary);
        preparedStatement.setDouble(3, maxSalary);

        // "'" -> \"\'\"
        // 'Assistant of Assistant Manager' or job_id >= 0
//        int rowsInserted = statement.executeUpdate("INSERT INTO jobs (job_title, min_salary, max_salary) " +
//                "VALUES(\'" + title + "\', " + minSalary + ", " + maxSalary +")");

        int rowsInserted = preparedStatement.executeUpdate();

        System.out.println("Rows inserted: " + rowsInserted);

//        connection.commit();
    }

    private void insertBulkJobs() throws SQLException {
//        Statement statement = connection.createStatement();
        int rowsInserted = statement.executeUpdate("INSERT INTO jobs (job_title, min_salary, max_salary) " +
                "VALUES(\'Assistant Manager\', 5000.0, 10000.0)");

        System.out.println("Rows inserted: " + rowsInserted);
    }

    private void updateJob(String jobTitle, int jobId) throws SQLException {
//        Statement statement = connection.createStatement();
        PreparedStatement preparedStatement =
                connection.prepareStatement("UPDATE jobs SET job_title= ? WHERE job_id= ?");
        preparedStatement.setString(1, jobTitle);
        preparedStatement.setInt(2, jobId);
        int rowUpdated = preparedStatement.executeUpdate();
//        int rowUpdated =
//                statement.executeUpdate("UPDATE jobs SET job_title=\'Assistant of Assistant Manager\' WHERE job_id=28"); // DRY

        System.out.println("Rows updated: " + rowUpdated);

        connection.commit();
    }

    private void deleteJob(String jobTitle) throws SQLException {
//        Statement statement = connection.createStatement();
        PreparedStatement preparedStatement =
                connection.prepareStatement("DELETE FROM jobs WHERE job_title = ?");
        preparedStatement.setString(1, jobTitle);

        int rowDeleted = preparedStatement.executeUpdate();

//        int rowDeleted = statement.executeUpdate("DELETE FROM jobs WHERE job_title = \'Assistant Manager\'");
        System.out.println("Rows deleted: " + rowDeleted);

        connection.commit();

    }

}
