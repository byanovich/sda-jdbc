package ee.sda.jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseObjectsMapping {

    private Connection connection;

    public static void main(String[] args) throws SQLException {
        Job job = new Job();
        job.setTitle("New job");
        job.setMaxSalary(20000);
        job.setMinSalary(3000);

        DatabaseObjectsMapping databaseObjectsMapping = new DatabaseObjectsMapping();
        job = databaseObjectsMapping.insertJob(job);

        job.setMinSalary(10000);
        databaseObjectsMapping.updateJob(job);

        databaseObjectsMapping.deleteJob(job);

    }

    public DatabaseObjectsMapping() throws SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sda",
                "sdauser", "password");
    }

    private Job insertJob(Job job) throws SQLException {
        PreparedStatement preparedStatement =
                connection.prepareStatement("INSERT INTO jobs (job_title, min_salary, max_salary) "+
                        "VALUES(?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, job.getTitle());
        preparedStatement.setDouble(2, job.getMinSalary());
        preparedStatement.setDouble(3, job.getMaxSalary());

        int insertedRows = preparedStatement.executeUpdate();
        ResultSet resultSet = preparedStatement.getGeneratedKeys();
        if (resultSet.next()) {
            int id = resultSet.getInt(1);
            job.setId(id);
            System.out.println("New id = " + id);
        }

        if (insertedRows == 0) {
            throw new SQLException("Job was not inserted");
        }

        return job;
    }

    private void updateJob(Job job) throws SQLException {
        PreparedStatement preparedStatement =
                connection.prepareStatement("UPDATE jobs SET job_title = ?, min_salary = ?, max_salary = ? " +
                        "WHERE job_id = ?");

        preparedStatement.setString(1, job.getTitle());
        preparedStatement.setDouble(2, job.getMinSalary());
        preparedStatement.setDouble(3, job.getMaxSalary());
        preparedStatement.setInt(4, job.getId());

        int updatedRow = preparedStatement.executeUpdate();

        if (updatedRow == 0) {
            throw new SQLException("Job with job id = " + job.getId() + " was not updated");
        }

    }

    private void deleteJob(Job job) throws SQLException {
        PreparedStatement preparedStatement =
                connection.prepareStatement("DELETE FROM jobs WHERE job_id = ?");
        preparedStatement.setInt(1, job.getId());

        int deletedRows = preparedStatement.executeUpdate();

        if (deletedRows == 0) {
            throw new SQLException("The job with job id = " + job.getId() + " was not removed");
        }

    }

    private Job findJob(int id) throws SQLException {
        PreparedStatement preparedStatement =
                connection.prepareStatement("SELECT job_id, job_title, min_salary, max_salary FROM jobs " +
                        "WHERE job_id = ?");
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
            Job job = new Job();
            job.setId(resultSet.getInt(1));
            job.setTitle(resultSet.getString(2));
            job.setMinSalary(resultSet.getDouble(3));
            job.setMaxSalary(resultSet.getDouble(4));

            return job;
        }

        return null;
    }

    private List<Job> findAllJobs() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet result =
                statement.executeQuery("SELECT job_id, job_title, min_salary, max_salary FROM jobs");

        List<Job> jobs = new ArrayList<Job>();

        while (result.next()) {
            Job job = new Job();
            job.setId(result.getInt(1));
            job.setTitle(result.getString(2));
            job.setMinSalary(result.getDouble(3));
            job.setMaxSalary(result.getDouble(4));

            jobs.add(job);

        }

        return jobs;
    }
}
